// Dependencies and Modules
	const express = require('express')
	const controller = require('../controllers/users')
	const auth = require('../auth.js')
	const User = require('../models/User')

	//destructure the actual function that we need to use
	const { verify, verifyAdmin} = auth

// Routing Component
	const route = express.Router()

// POST/create user route
	route.post('/register', async (req, res) => {
		//res.send('Correct route for register')
		let userData = req.body

		const existEmail = await User.findOne({ email: req.body.email })

		if(!existEmail){
			controller.register(userData).then(result => res.send(result) )
		}else res.send({message: `Email is already taken!`})
		
	})

// Login Route
	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(result => res.send(result) )
	})

// Set user as Admin (Admin)
	route.put('/:userId/status', verify, verifyAdmin, (req, res) => {
		controller.setStatus(req.params.userId).then(result => res.send(result))
	})

//GET the user's details
	route.get('/details', verify, (req, res) => {
		//req.user is inside auth verify()
		controller.getProfile(req.user.id).then(result => res.send(result) )
	})

//Change user's password
	// route.post('/:userId/password', verify, (req, res) => {
	// 	controller.changePass(req.params.userId, req.body).then(result => res.send(result))
	// })

	route.post('/:userId/password', verify, controller.changePass)


// Expose Route System
	module.exports = route