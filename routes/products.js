// Dependencies and Modules
	const express = require('express')
	const controller = require('../controllers/products')
	const auth = require('../auth')
	const Product = require('../models/Product')

	const multer = require('multer')
	


	const storage = multer.diskStorage({
		destination: function(req, file, cb){
			cb(null, './uploads');
		},
		filename: function(req, file, cb){
			cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname)
		}
	})

	const fileFilter = (req, file, cb) => {

		if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || 'image/png'){
			cb(null, true);
		}else{
			cb(null, false);
		}
		
	}
	const upload = multer({
		storage: storage, 
		limits: {
		fileSize: 5000000
		},
		fileFilter: fileFilter
	})

	//destructure the actual function that we need to use
	const { verify, verifyAdmin} = auth

// Routing Component
	const route = express.Router()

// Add a Product Route (ADMIN only)
	route.post('/addProduct', verify, verifyAdmin, upload.single('productImage'), async (req, res) => {
		const existProduct = await Product.findOne({ name: req.body.name})
		if(!existProduct){
			controller.addProduct(req.body, req.file).then(result => res.send(result) )
		}else res.send({message: `Product is already on sale`})

		
	})

// Get All Products
	route.get('/all', (req, res) => {
		controller.getAllProducts().then(result => res.send(result))
	})

// Get All Active Products
	route.get('/active', (req, res) => {
		controller.getActiveProducts().then(result => res.send(result) )
	})

// Get All InActive Products
	route.get('/inactive', (req, res) => {
		controller.getInactiveProducts().then(result => res.send(result) )
	})

// Get Specific Product
	route.get('/:productId', (req, res) => {
		controller.getProduct(req.params.productId).then(result => res.send(result) )
	})

// Update Product Information (ADMIN only)
	route.put('/:productId/update', verify, verifyAdmin, upload.single('productImage'), (req, res) => {
		controller.updateProduct(req.params.productId, req.body, req.file).then(result => res.send(result))
	})

// Archive Product (ADMIN only)
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		controller.archiveProduct(req.params.productId, req.body).then(result => res.send(result))
	})

// Activate Product (ADMIN only)
	route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
		controller.activateProduct(req.params.productId, req.body).then(result => res.send(result))
	})

// Expose Route System
	module.exports = route