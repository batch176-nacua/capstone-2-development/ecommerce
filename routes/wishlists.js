// Dependencies and Modules
	const express = require('express') 
	const controller = require('../controllers/wishlists') 
	const auth = require('../auth.js')

// Routing Component
	//Implement a new routing system dedicated for the users collection
	const route = express.Router()

// Add Wishlist Route
	route.post('/addWishlist', auth.verify, controller.addWishlist)

// Get Logged User's Pending Wishlist
	route.get('/myWishlist', auth.verify, controller.getWishlist)

// Checkout Specific Wishlist Route
	route.post('/:wishlistId/buy', auth.verify, controller.buyWishlist)

// Delete All Not Pending Wishlist to remove junk db data
	route.delete('/delete', auth.verify, auth.verifyAdmin, controller.deleteWishlist)
	
	



// Expose Route System
	module.exports = route