// Dependencies and Modules
	const express = require('express') 
	const controller = require('../controllers/orders') 
	const auth = require('../auth.js')

// Routing Component
	//Implement a new routing system dedicated for the users collection
	const route = express.Router()

// Checkout Route
	route.post('/checkout', auth.verify, controller.checkout)

// Get Logged User's Order
	route.get('/myOrder', auth.verify, controller.getOrder)

// Retrieve ALL Orders (Admin)
	route.get('/allOrders', auth.verify, auth.verifyAdmin, (req, res) => {
		controller.getAllOrders().then(result => res.send(result))
	})

// Retrieve Most Expensive Order
	route.get('/highest', (req, res) => {
		controller.getHighestOrder().then(result => res.send(result) )
	})

// Expose Route System
	module.exports = route