// Dependencies and Modules
	const User = require('../models/User') 
	const Product = require('../models/Product')
	const Order = require('../models/Order')

// Checkout
	module.exports.checkout = async (req, res) => {
		if(!req.user.isAdmin){
			const newOrder = new Order({
				userId: req.user.id,
				products: [],
				totalAmount: 0
			})
			for(let i = 0; i<req.body.length; i++){
				await Product.findById(req.body[i].productId).then(product => {
					if(product){
						if(product.isActive){
							let newProduct = {
								productId: req.body[i].productId,
								productName: product.name,
								quantity: req.body[i].quantity
							}
						newOrder.totalAmount += req.body[i].quantity * product.price
						newOrder.products.push(newProduct)	
						}else return res.send({message: 'Product is not on sale'})

					}else return res.send({message: 'Product dos not exist'})
				})
			}
			newOrder.save().then(result => {
				console.log(result)
				return res.send(result)
			}).catch(error => res.send(error))
				
		}else return res.send({message: "Action Forbidden: Must not be an Admin"})

	}


// Logged User's Order
	module.exports.getOrder = (req, res) => {
		Order.find( {'userId': req.user.id} ).then(result => {
			res.send(result)
		}).catch(error => error.message)
	}

// Retrieve ALL Orders (Admin)
	module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result
		})
	}

// Retrieve Most Expensive Order
	module.exports.getHighestOrder = () => {
		return Order.find().sort({totalAmount: -1}).limit(1).then(result => {
			return result
		})
	}



