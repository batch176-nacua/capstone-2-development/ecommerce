// Dependencies and Modules
	const User = require('../models/User')
	const bcrypt = require('bcrypt')
	const dotenv = require('dotenv')
	const auth = require('../auth.js')

// Environment Variables Setup
	dotenv.config()
	let salt = parseInt(process.env.SALT)

// Functionalities [CREATE]
	//Register New Acc
	module.exports.register = (userData) => {
		let newUser = new User({
			email: userData.email,
			password: bcrypt.hashSync(userData.password, salt)
		})

		return newUser.save().then((success, err) => {
			if(success){
				return true
			}else return {message: 'Failed to Register Account'}
		})
	}

// User Authentication
	module.exports.loginUser = (data) => {
		return User.findOne({ email: data.email }).then(result => {
			if(result == null){
				return false
			}else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

				if(isPasswordCorrect){
					return { accessToken: auth.createAccessToken(result.toObject() )}
				}else return false
			}
		})
	}

// Set User status to Admin or vice versa (Admin)
	module.exports.setStatus = (userId) => {
		
		return User.findById(userId).then( user => {
			//return user
			if(user){
				if(!user.isAdmin){
					let updateStatus = {
						isAdmin: true
					}
					return User.findByIdAndUpdate(userId, updateStatus).then((success, error) => {
						if(error) {
							return false
						} else return {message: "User Status: Admin"}
					})
				}else {
					let updateStatus = {
						isAdmin: false
					}
					return User.findByIdAndUpdate(userId, updateStatus).then((success, error) => {
						if(error) {
							return false
						} else return {message: "User Status: Not Admin"}
					})
				}

			}else return {message: "User ID does not exist"}
		})
	}

//GET User's Detail
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			//Change the value of the user's password to an empty string
			result.password = ''
			return result
		})
	}

//Change Password
	// module.exports.changePass = (id, body) => {
	// 	const {currentPass, newPass} = body;

	// 	return User.findByIdAndUpdate(id).then(user => {

	// 		bcrypt.compare(currentPass, user.password, (err, isMatch) => {
	// 			console.log(isMatch)
	// 			if(isMatch){
	// 				user.password = bcrypt.hashSync(newPass, salt)
	// 				user.save()
	// 				return true
	// 			}else return {message: "Does not match"}
	// 		})

	// 	})
	// }

	module.exports.changePass = (req, res) => {
		User.findById(req.params.userId).then(user => {
			//res.send(user.email)
			bcrypt.compare(req.body.currentPass, user.password, (err, isMatch) => {
				if(isMatch){
					user.password = bcrypt.hashSync(req.body.newPass, salt)
					user.save()
					res.send(true)
				}else res.send(false)
			})
		})
	}

