// Dependencies and Modules
	const User = require('../models/User') 
	const Product = require('../models/Product')
	const Order = require('../models/Order')
	const Wishlist = require('../models/Wishlist')

// Add Wishlist 
	module.exports.addWishlist = async (req, res) => {
		if(!req.user.isAdmin){
			const newWishlist = new Wishlist({
				userId: req.user.id,
				products: [],
				totalAmount: 0
			})
			for(let i = 0; i<req.body.length; i++){
				await Product.findById(req.body[i].productId).then(product => {
					if(product){
						if(product.isActive){
							let newProduct = {
								productId: req.body[i].productId,
								productName: product.name,
								quantity: req.body[i].quantity
							}
						newWishlist.totalAmount += req.body[i].quantity * product.price
						newWishlist.products.push(newProduct)	
						}else return res.send({message: 'Product is not on sale'})

					}else return res.send({message: 'Product dos not exist'})
				})
			}
			newWishlist.save().then(result => {
				return res.send(result)
			}).catch(error => res.send(error))
				
		}else return res.send({message: "Action Forbidden: Must not be an Admin"})

	}

// Logged User's Pending Wishlist
	module.exports.getWishlist = (req, res) => {
		Wishlist.find( {'userId': req.user.id} && {isPending: true} ).then(result => {
			res.send(result)
		}).catch(error => error.message)
	}

// Checkout Wishlist
	module.exports.buyWishlist = async (req, res) => {
		
		let isCheckoutWish = await Wishlist.findById(req.params.wishlistId).then(wishlist => {
			if(wishlist.isPending){
				newOrder = new Order ({
					userId: req.user.id,
					products: wishlist.products,
					totalAmount: wishlist.totalAmount
				})
				
				newOrder.save()
				return true
			}else res.send({message: `You already bought this wishlist!`})
			
		})

		let updateStatus = {
			isPending: false
		}

		let isUpdateStat = await Wishlist.findByIdAndUpdate(req.params.wishlistId, updateStatus).then((success, err) => {
			if(err) {
				return false
			} else return true
		}).catch(error => error.message)

		if(isCheckoutWish && isUpdateStat){
			res.send({message: `Checkout Wishlist`})
		}

	}

// Delete ALL not pending Wishlist to avoid junk db data
	module.exports.deleteWishlist = async (req, res) => {
		Wishlist.remove({ isPending: false }).then(result => {
			res.send( {message: `Successfully Deleted`})
		}).catch(error => error.message)
	}




