// Dependencies and Modules
	const Product = require('../models/Product')

	const multer = require('multer')

	// let query = Product.find()
	// query.count(function (err, success) {
	//     if (err) console.log(err)
	//     else {
	//     	count = success
	//     }
	// })
	const storage = multer.diskStorage({
		destination: function(req, file, cb){
			cb(null, './uploads/');
		},
		filename: function(req, file, cb){
			cb(null, new Date().toISOString() + file.originalname)
		}
	})

	const upload = multer({storage: storage})

// Add a Product
	module.exports.addProduct = (data, file) => {
		let newProduct = new Product({
			name: data.name,
			description: data.description,
			category: data.category,
			price: data.price,
			productImage: file.path
		})
		
		return newProduct.save().then((success, err) => {
			if(err){
				return false
			}else return true
		}).catch(error => error.message)

	}

// Retrieve ALL products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result
		})
	}
	
// Retrieve ALL ACTIVE products
	module.exports.getActiveProducts = () => {
		return Product.find({isActive: true}).then(result => {
			return result
		})
	}

// Retrieve ALL INACTIVE products
	module.exports.getInactiveProducts = () => {
		return Product.find({isActive: false}).then(result => {
			return result
		})
	}

// Retrieve SPECIFIC product
	module.exports.getProduct = (data) => {
		return Product.findById(data).then(result => {
			return result
		})
	}

// Update SPECIFIC product
	module.exports.updateProduct = (productId, data, file) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			category: data.category,
			price: data.price,
			productImage: file.path
		}

		return Product.findByIdAndUpdate(productId, updatedProduct).then((success, err) => {
			if(err){
				return false
			} else return true
		}).catch(error => error.message)
	}

// Archive SPECIFIC product
	module.exports.archiveProduct = (productId, data) => {
		let updateStatus = {
			isActive: false
		}

		return Product.findByIdAndUpdate(productId, updateStatus).then((success, err) => {
			if(err){
				return false
			}else return true
		}).catch(error => error.message)
	}

// Activate SPECIFIC product
	module.exports.activateProduct = (productId, data) => {
		let updateStatus = {
			isActive: true
		}

		return Product.findByIdAndUpdate(productId, updateStatus).then((success, err) => {
			if(err){
				return false
			}else return true
		}).catch(error => error.message)
	}
	