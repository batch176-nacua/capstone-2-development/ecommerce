// Modules and Dependencies
	const mongoose = require('mongoose')

// Schema
	const wishlistSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, 'User ID is required']
		},
		products: [
			{
				productId: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Product',
					required: [true, 'Product ID is required']
				},
				productName: {
					type: String
				},
				quantity: {
					type: Number,
					default: 1
				}
			}
		],
		totalAmount: {
			type: Number
		},
		isPending: {
			type: Boolean,
			default: true
		},
		dateCreated: {
			type: Date,
			default: new Date()
		}
	})

// Model
	module.exports = mongoose.model('Wishlist', wishlistSchema)