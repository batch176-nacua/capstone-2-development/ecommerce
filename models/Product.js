// Modules and Dependencies
	const mongoose = require('mongoose')

// Schema/Template/Blueprint
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Product name is required']
		},
		productImage: {
			type: String,
			required: [true, 'Image is required']
		},
		description: {
			type: String,
			required: [true, 'Description is required']
		},
		category: {
			type: String,
			required: [true, 'Category is required']
		},
		price: {
			type: Number,
			required: [true, 'Price is required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	})

// Model
	module.exports = mongoose.model('Product', productSchema)