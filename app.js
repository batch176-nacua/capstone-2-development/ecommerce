// Dependencies and Modules
	const express = require('express')
	const mongoose = require('mongoose')
	const dotenv = require('dotenv')
	const cors = require('cors')

	const userRoutes = require('./routes/users')
	const productRoutes = require('./routes/products')
	const orderRoutes = require('./routes/orders')
	const wishlistRoutes = require('./routes/wishlists')

// Environment Setup	
	dotenv.config()
	const port = process.env.PORT
	let atlas = process.env.ATLAS

// Server Setup
	const app = express()

	app.use(express.json())

	//It enables all origins/address/URL of the client request
	app.use(cors())
	app.use('/uploads', express.static('uploads'))


// Database Connection
	mongoose.connect(atlas)
	const connectStatus = mongoose.connection
	connectStatus.once('open', () => console.log(`MongoDB Successfully Connected`))

// Backend Routes
	app.use('/users', userRoutes)
	app.use('/products', productRoutes)
	app.use('/orders', orderRoutes)
	app.use('/wishlist', wishlistRoutes)

// Server Gateway Response
	app.listen(port, () => {
		console.log(`API is Hosted on Port : ${port}`)
	})

	app.get('/', (req, res) => {
		res.send(`Welcome to Capstone 2`)
	})

